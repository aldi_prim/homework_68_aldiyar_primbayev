class Food < ApplicationRecord

  belongs_to :restaurant

  has_one_attached :image
  validates :image, file_content_type: { allow: ['image/jpeg', 'image/gif', 'image/png'] }


  validates :name, presence: true, length: {
    maximum: 50
  }

  validates :price, presence: true, numericality: {
    greater_than_or_equal_to: 0
  }

  validates :desc, presence: true, length: {
    maximum: 250
  }

end
