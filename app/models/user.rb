class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :name, presence: true, length: { maximum: 50 }
  validates :phone, presence: true, numericality: true, length: { minimum: 7, maximum: 15 }
  validates :adress, presence: true, length: { maximum: 50 }

end
