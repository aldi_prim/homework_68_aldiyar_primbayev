class Restaurant < ApplicationRecord

  has_many :foods, dependent: :destroy

  has_one_attached :image
  validates :image, file_content_type: { allow: ['image/jpeg', 'image/gif', 'image/png'] }

  validates :name, presence: true, length: {
    maximum: 50
  }

  validates :description, presence: true, length: {
    maximum: 250
  }

end
