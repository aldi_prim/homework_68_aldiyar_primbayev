ActiveAdmin.register Food do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :name, :desc, :restaurant_id, :price, :image

  index do
    selectable_column
    id_column
    column :image do |food|
      if food.image.attached?
        image_tag food.image.variant(combine_options: {
          gravity: 'Center',
          crop: '50x50+0+0'
        })
      end
    end
    column :name
    column :restaurant
    column :price
    actions
  end

  filter :name
  filter :restaurant

  form do |f|
    f.inputs do
      f.input :name
      f.input :price
      f.input :desc
      f.input :restaurant
      if food.image.attached?
        f.input :image,
          as: :file,
          hint: image_tag(
            url_for(
              f.object.image.variant(combine_options: {
                gravity: 'Center',
                crop: '250x250+0+0'
              })
            )
          )
      else
        f.input :image, as: :file
      end
    end
    f.actions
  end

  show do
    attributes_table do
      row :image do |food|
        if food.image.attached?
          image_tag food.image.variant(combine_options: {
            gravity: 'Center',
            crop: '550x550+0+0'
          })
        end
      end
      row :name
      row :price
      row :desc
      row :restaurant
    end
    active_admin_comments
  end

end
