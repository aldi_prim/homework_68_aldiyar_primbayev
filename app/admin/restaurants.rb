ActiveAdmin.register Restaurant do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  permit_params :name, :description, :image

  index do
    selectable_column
    id_column
    column :image do |restaurant|
      if restaurant.image.attached?
        image_tag restaurant.image.variant(combine_options: {
          gravity: 'Center',
          crop: '50x50+0+0'
        })
      end
    end
    column :name
    actions
  end

  filter :name

  form do |f|
    f.inputs do
      f.input :name
      f.input :description
      if restaurant.image.attached?
        f.input :image,
          as: :file,
          hint: image_tag(
            url_for(
              f.object.image.variant(combine_options: {
                gravity: 'Center',
                crop: '250x250+0+0'
              })
            )
          )
      else
        f.input :image, as: :file
      end
    end
    f.actions
  end

  show do
    attributes_table do
      row :image do |restaurant|
        if restaurant.image.attached?
          image_tag restaurant.image.variant(combine_options: {
            gravity: 'Center',
            crop: '550x550+0+0'
          })
        end
      end
      row :name
      row :description
    end
    active_admin_comments
  end

end
